#include <iostream>
#include <vector>
#include <string>
#include "Demo.h"

Demo fetchDemo(int m, int n)
{
    return {m, n};
}



int main() {
    std::cout << "C++ 11!" << std::endl;

    // C++ 11 新初始化方式
    int array[5]{4,5,66,7,0};
    std::cout << array[0] << std::endl;

    std::vector<int> vectors{1, 5, 1};
    std::cout << vectors.at(0) << std::endl;
    std::string str{"Hello world"};

    std::cout << str << std::endl;

    int *pointer = new int[5]{1,4,5};
    pointer += 2;
    std::cout << *pointer << std::endl;

    Demo demo = fetchDemo(9, 10);
    demo.getIndex();
    // 类成员可以拥有初始值
    std::cout << demo.getDefaultValue() << std::endl;

    // auto 关键字, auto 必须初始化
    auto p = new Demo{10, 42};
    auto k = 3455LL; // Long long
    std::cout <<  p->getDefaultValue()  << std::endl;
    std::cout << k << std::endl;

    // 智能指针

    // nullptr , NULL == nullptr

    bool b = nullptr; // 转换为 false
    std::cout << b << std::endl;

    std::cout << "for range" << std::endl;
    // 基于范围的 for 循环
    for (int element : array) {
        std::cout << element << std::endl;
    }

    std::cout << "auto v for vector" << std::endl;
    for (auto &v : vectors) {
        v = 1;

    }

    for (auto f : vectors) {
        std::cout << f << std::endl;
    }
    return 0;
}